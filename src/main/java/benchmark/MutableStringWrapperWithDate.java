package benchmark;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MutableStringWrapperWithDate {

  protected String value;

  public MutableStringWrapperWithDate(String value) {
    super();
    String date = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
    String folderName = "Experiment_" + date + "/";
    this.value = value + folderName;
    try {
      Files.createDirectories(Paths.get(this.value));
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public MutableStringWrapperWithDate() {
    this("");
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return value.toString();
  }

}
